console.log("Hola");

/*
Defina objetos similares para los otros meses. 
Para renderizar la tabla de un mes basta saber 
cual es el día de semana inicial, y cuántos días tiene el mes.
*/

var diasMayo = [{"firstWeekDay": 6, "days": 31},{"firstWeekDay": 2, "days": 28},{"firstWeekDay": 2, "days": 31},{"firstWeekDay": 5, "days": 30},{"firstWeekDay": 0, "days": 31},{"firstWeekDay": 3, "days": 30},{"firstWeekDay": 5, "days": 31},{"firstWeekDay": 1, "days": 31},{"firstWeekDay": 4, "days": 30},{"firstWeekDay": 6, "days": 31},{"firstWeekDay": 2, "days": 30},{"firstWeekDay": 4, "days": 31}]
var eventosMayo = [[],[],[],[],[],[],[],[],[],[],[],[]];


function addEventToDay(month, day) {
    var monthNumber = getMonthNumber(month);
    console.log(month + " " + day);
    var eventText = window.prompt("Ingrese texto del evento: ");

    /* El chequeo de eventText es obligatorio */
    if(eventText) {
	eventosMayo[monthNumber][day] = eventText;
    }

    renderSelectedMonth(month);
}
		    
function getHTMLForMonthDay(month, day) {
    var monthNumber = getMonthNumber(month);
    var dayId = day;
    var dayHTML = "<td id=\"" + dayId + "\" ";
    dayHTML += "onclick=\"addEventToDay(" + month + "," + day + ")\">";
    dayHTML += day;

    if(eventosMayo[monthNumber][day]) {
	dayHTML += "<hr/>";
	dayHTML += eventosMayo[monthNumber][day];
    }
	
    dayHTML += "</td>";
    return dayHTML;
}


function getHTMLForMonthTable(selectedMonth) {
    /*
     El valor de weekDayIdx debe cambiar
     si el primer dia del mes  no es lunes
    */
   
    var monthNumber = getMonthNumber(selectedMonth);
    var weekDayIdx = diasMayo[monthNumber].firstWeekDay;
    var newHTML = "";
    var newSemanaHTML = ""; 
    var passage = 0;  
    
    for(var i = 1; i <= diasMayo[monthNumber].days; i++) {
	if(weekDayIdx == 0) {
        newSemanaHTML += "<tr>";
        var passage = 1;
	}
    if(weekDayIdx > 0 && weekDayIdx < 7 && passage == 0) {
        newSemanaHTML += "<tr>";
        for(var j=1; j<= diasMayo[monthNumber].firstWeekDay; j++){
            newSemanaHTML += "<td> </td>";
        }
        var passage=1;
	}
    
	newSemanaHTML += getHTMLForMonthDay(selectedMonth, i);
    
	if(weekDayIdx == 6 || (i + 1) > diasMayo[monthNumber].days) {
	    newSemanaHTML += "</tr>";
	    newHTML += newSemanaHTML;
	    newSemanaHTML = "";
	}
	
	weekDayIdx = (weekDayIdx + 1) % 7;	
    }

    return newHTML;
}



function getMonthName(month) {
    switch(parseInt(month)) {
    case 0: return "Enero";
    case 1: return "Febrero";
    case 2: return "Marzo";
    case 3: return "Abril";
    case 4: return "Mayo";
    case 5: return "Junio";
    case 6: return "Julio";
    case 7: return "Agosto";
    case 8: return "Septiembre";
    case 9: return "Octubre";
    case 10: return "Noviembre";
    case 11: return "Diciembre";
    }

    throw new Error("No existe mes con indice: " + month);
}

function getMonthNumber(month) {
    switch(parseInt(month)) {
    case 0: return 0;
    case 1: return 1;
    case 2: return 2;
    case 3: return 3;
    case 4: return 4;
    case 5: return 5;
    case 6: return 6;
    case 7: return 7;
    case 8: return 8;
    case 9: return 9;
    case 10: return 10;
    case 11: return 11;
    }

    throw new Error("No existe mes con indice: " + month);
}

function Borrar(nombre) {
        eventosMayo[nombre]=[]

        renderSelectedMonth(nombre)

}

function renderSelectedMonth(selectedMonth) {
    console.log(selectedMonth);
    var monthName = getMonthName(selectedMonth);
    console.log(monthName);
    monthNumber = getMonthNumber(selectedMonth)
    // Ver: http://stackoverflow.com/questions/13775519/html-draw-table-using-innerhtml
    var newHTML = "";
    newHTML += "<h1>Mes: " + monthName + "</h1>";
    newHTML += "<table>";    
    newHTML += "<tr>";
    newHTML += "<th>Lunes</th>";
    newHTML += "<th>Martes</th>";
    newHTML += "<th>Miercoles</th>";
    newHTML += "<th>Jueves</th>";
    newHTML += "<th>Viernes</th>";
    newHTML += "<th>Sabado</th>";
    newHTML += "<th>Domingo</th>";
    newHTML += "</tr>";

    /*
    Aqui usted debe renderizar la tabla con los dias del mes
    Utilice la información en el objeto 'diasMayo' o el que
    corresponda según el mes seleccionado.

    A modo de demostración, el siguiente código renderiza
    la tabla con el mes de Mayo.
    */

    newHTML += getHTMLForMonthTable(selectedMonth);

    /* Fin de la tabla */
    newHTML += "</table>";

    newHTML += "<br>"
    newHTML += "<button onclick=\"Borrar(monthNumber)\"> Borrar eventos de este mes"
    newHTML += "</button>"
    
    /* Actualizamos el innerHTML del div currentMonth */
    var monthDiv = document.getElementById("currentMonth");    
    monthDiv.innerHTML = newHTML;
}
