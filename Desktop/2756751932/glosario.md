1. **Control de versiones (VC)**
    * Servidor central que contiene todos los archivos en sus versiones y clientes que pueden extraer archivos de este repositorio central
    https://git-scm.com/book/fr/v1/D%C3%A9marrage-rapide-%C3%80-propos-de-la-gestion-de-version
2. **Control de versiones distribuido (DVC)**
    * En un DVC, los clientes no solo extraen la última versión de un archivo, sino que duplican completamente el repositorio
    https://git-scm.com/book/fr/v1/D%C3%A9marrage-rapide-%C3%80-propos-de-la-gestion-de-version
3. **Repositorio remoto y Repositorio local**
    * Los repositorios remotos son versiones de su proyecto que están alojadas en Internet o en la red corporativa, al contrario de los locales.
    https://git-scm.com/book/fr/v2/Les-bases-de-Git-Travailler-avec-des-d%C3%A9p%C3%B4ts-distants
4. **Copia de trabajo / Working Copy**
    * Un depósito localizado completo, como un clon,
     https://www.thomas-krenn.com/en/wiki/Git_Basic_Terms
5. **Área de Preparación / Staging Area**
    * El área de preparación es un archivo simple, que está contenido en su directorio Git
    https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Rudiments-de-Git
6. **Preparar Cambios / Stage Changes**
    * Afectar a todos los archivos modificados y sin seguimiento
    https://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git
7. **Confirmar cambios / Commit Changes**
    * Guardar tus cambios en el repositorio local
    https://www.git-tower.com/learn/git/commands/git-commit
8. **Commit**
    * Registrar cambios en el repositorio
    https://git-scm.com/docs/git-commit
9. **clone**
    * Una utilidad de línea de comandos de Git que se utiliza para apuntar a un repositorio existente y crear una copia o copia del repositorio de destino.
    https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone
10. **pull**
    * Combinación de otros dos comandos, git fetch seguido de git merge 
    https://www.atlassian.com/git/tutorials/syncing/git-pull
11. **push**
    * Un comando para publicar nuevas confirmaciones locales en un servidor remoto
    https://www.git-tower.com/learn/git/commands/git-push
12. **fetch**
    * Un comando para descargar confirmaciones, archivos y referencias de un repositorio remoto a su repositorio local
    https://www.atlassian.com/git/tutorials/syncing/git-fetch
13. **merge**
    * Este comando se usa para incorporar cambios de otro repositorio y se puede usar a mano para combinar cambios de una rama a otra
    https://git-scm.com/docs/git-merge
14. **status**
    * Muestra el estado del directorio de trabajo y el área de preparación.
    https://www.atlassian.com/git/tutorials/inspecting-a-repository
15. **log**
    * Muestra los registros de confirmación
    https://git-scm.com/docs/git-log
16. **checkout**
    * El acto de cambiar entre diferentes versiones de una entidad objetivo
    https://www.atlassian.com/git/tutorials/using-branches/git-checkout
17. **Rama / Branch**
    * Un puntero móvil ligero a uno de estos compromisos
    https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Les-branches-en-bref
18. **Etiqueta / Tag**
    * Las etiquetas son referencias que apuntan a puntos específicos en la historia de Git.
    https://www.atlassian.com/git/tutorials/inspecting-a-repository/git-tag
