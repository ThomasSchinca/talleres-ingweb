1. **Glosario**

    **Request**
        * Permite acceder a la informacion que pasa del cliente al servidor
    **Response**
        * Lo que da el servidor al cliente siguiente de una request
    **Status Codes**
        * Indicacion intuitiva del estatus
    **Methods: GET, POST, HEAD, OPTIONS, PUT, DELETE**
        * Get : Representación del recurso específico
        * Post : Enviar una entidad al recurso indicado
        * Head : Lo mismo que Get pero unicamente para la cabeza, sin el cuerpo
        * Options : Describir las optiones de comunicacion con el recurso visado
        * Put : Reemplaza todas las entidades con el contenido de la request
        * Delete : Elimina el recurso indicado
    **Header: Accept, Accept-Charset, Accept-Encoding**
        * Accept : Indica el tipo de contenidos que el cliente va a ser capaz de interpretar
        * Accept-Charset : Indica el tipo de char que el cliente va a ser capaz de comprender
        * Accept-Encoding : Permite de definir lo  que va a ser la codificación del contenido 
    **Cache-Control**
        * Encabezado usado para definir la politica de caches de los  contenidos
    **Connection**
        * Controla si la conexión se queda abierta o no cuando la transacción esta terminada
    **Cookie, Set-Cookie**
        * El encabezado Cookie contiene cookies HTTP almacenadas previamente enviadas por el servidor con el encabezado Set-Cookie. El encabezado de la cookie es opcional y se puede omitir
    **Host**
        * El encabezado Host especifica el nombre de dominio del servidor y, opcionalmente, el número de puerto TCP en el que el servidor está escuchando
    **Origin**
        * El encabezado Origin indica de dónde se origina una recuperación. No incluye ninguna información de ruta, solo el nombre del servidor
    **Referer**
        * El parámetro encabezado Referer contiene la dirección de la página visitada anteriormente en la que se siguió un enlace
    **User-Agent**
        * La solicitud de cabecera del Agente de Usuario contiene una cadena característica que permite identificar el protocolo de red  que ayuda a descubrir  el tipo de aplicación, sistema operativo, provedor del software o laversión del software de la petición del agente de usuario.
    **Content-Encoding, Content-Length, Content-Type**
        * La cabecera Content-Encoding es usada para comprimir el media-type. El encabezado de entidad Content-Length indica el tamaño de la entidad-cuerpo, en bytes, enviado al destinatario. El encabezado Content-Type se utiliza para indicar el tipo de medio del recurso.
    **Location** 
        * El encabezado de respuesta de ubicación indica la URL para redirigir a una página
    **Upgrade**
        * El encabezado Upgrade le permite al cliente especificar qué protocolos de comunicación adicionales admite y le gustaría usar si el servidor considera apropiado cambiar de protocolo

2. |REQ/RES|Método HTTP (solo REQ)|URL|Headers (sólo nombres)|Status (solo RES)|Descripción|
|---|----------------|---------|---|---|---|
| REQ | GET  |  http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/bootstrap/fonts/glyphicons-halflings-regular.woff2 | User-Agent, Origin, Accept  |  n/a  | Request al url  |
|  RES |  n/a  | n/a   |  Date, Server, Last-Modified, ETag, Accept-Ranges, Content-Length, Content-Type |  200 | Se devuelve correctamente el sitio.  |


5. “Al ingresar la url en el navegador, el servidor web respondió correctamente con el
contenido, a pesar de no encontrarse inicialmente en cache.” Eso significa que la respuesta funcioná y que no había nada en la memoria cache, entonces es posible que fue la primera vez que el cliente hace este request.

6. |REQ/RES|Método HTTP (solo REQ)|URL|Headers (sólo nombres)|Status (solo RES)|Descripción|
|---|---|---------|---|---|---|
|REQ|GET|https://www.utt.fr/|Upgrade-Insecure-Requests, User-Agent, Accept|n/a|Primera request|
|RES|n/a|n/a|Date, Server, Vary, Content-Encoding, Keep-Alive, Connection, Transfer-Encoding, Content-Type|200|El servidor web respondió correctamente con el contenido|
|REQ|GET|https://www.utt.fr/wro/styles/fab13f3190ecd63073946f662fa782915fc93a2a.css|User-Agent, Accept|n/a|Request del style de la pagina en CSS|
|RES|n/a|n/a|Date, Server, cache-control, expires, Last-Modified, ETag, Content-Encoding, Vary, Content-Length, Content-Type|200|El servidor web respondió correctamente con el contenido|
|REQ|GET|https://www.utt.fr/uas/utt/SURCHCSS/surcharge_final.css|User-Agent, Accept|n/a|Request del override CSS|
|RES|n/a|n/a|Date, Server,Content-Disposition, Keep-Alive, Connection, Content-Encoding, Vary, Transfer-Encoding, Content-Type|200|El servidor web respondió correctamente con el contenido|
|REQ|GET|https://www.utt.fr/wro/scripts/51e8c92bcce8ddd5806d28e98c1083433991e542.js|User-Agent, Accept|n/a|Request del Javascript de la pagina|
|RES|n/a|n/a|Date, Server, cache-control, expires, Last-Modified, ETag, Content-Encoding, Vary, Content-Length, Content-Type|200|El servidor web respondió correctamente con el contenido|
|REQ|GET|https://www.utt.fr/uas/utt/LOGO/Logo.png|User-Agent, Accept|n/a|Request de una imagnen (el logo de la universidad)|
|RES|n/a|n/a|Date, Server,Content-Disposition, Keep-Alive, Connection, Content-Lenght, Vary, Content-Type|200|El servidor web respondió correctamente con el contenido|

Cuando miramos una interacción completa, podemos ver que la primera request es para conectarse (Upgrade-Insecure-Requests) y las siguientes para cargar el css, el override css, el javascript y finalmente todas las imaginas jpeg o png. 